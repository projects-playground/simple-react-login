import * as React from 'react';
import {hasAccount} from './helper';
import users from '../../pages/login/accounts.json'

const AuthContext = React.createContext();

const authReducer = (state, action) => {
    switch (action.type) {
        case 'login': {
            return (hasAccount(state.users, action.payload)) ? 
            {
                ...state,
                isLoggedIn: true,
                user: action.payload,
                errorMessage: null
            }:
            {
                ...state,
                isLoggedIn: false,
                user: action.payload,
                errorMessage: 'Please verify you entered the correct email and password'
            }
        }
        case 'logout': return {
            ...state,
            isLoggedIn: false,
            errorMessage: null
        }
        default: {
            throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}
const AuthProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(authReducer, {
        users,
        isLoggedIn: null
    })
    const value = { state, dispatch }
    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

function useAuth() {
    const context = React.useContext(AuthContext)
    if (context === undefined) {
        throw new Error('useAuth must be used within a AuthProvider')
    }
    return context
}
export { AuthProvider, useAuth }