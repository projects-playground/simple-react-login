import { render, fireEvent } from '@testing-library/react';
import LoginPage from '../LoginPage';
import { AuthProvider } from '../../../shared/auth/auth-context';

const setup = () => {
  const utils = render(<AuthProvider><LoginPage /></AuthProvider>);
  const emailInput = utils.getByTestId('inputUsername');
  const passwordInput = utils.getByTestId('inputPassword');

  return {
    emailInput,
    passwordInput,
    ...utils
  };
};

test('Pristine form: should not display any error messages', () => {
  const { errorMessagePassword, errorMessageUser } = setup();

  expect(errorMessagePassword).not.toBeDefined();
  expect(errorMessageUser).not.toBeDefined();
});

test('User Input: should not display error if username value has correct email format', () => {
  const { emailInput, queryByTestId } = setup();
  const value = 'test@gmail.com';
  fireEvent.change(emailInput, { target: { value: value } });

  expect(emailInput.value).toBe(value);
  expect(queryByTestId('errorMessageUser')).toBeNull();
});

it('User Input: should display error if username value does not have correct email format', () => {
  const { emailInput, getByTestId } = setup();
  const value = 'test';
  const expected = 'Invalid email address';
  fireEvent.change(emailInput, { target: { value: value } });

  expect(emailInput.value).toBe(value);
  expect(getByTestId('errorMessageUser').textContent).toBe(expected);
});

test('Password Input: should not display error if password value is has valid format', () => {
  const { passwordInput, queryByTestId } = setup();
  const value = 'Passw0rd';
  fireEvent.change(passwordInput, { target: { value: value } });

  expect(passwordInput.value).toBe(value);
  expect(queryByTestId('errorMessagePassword')).toBeNull();
});

test('Password Input: should display error if password value is has invalid format', () => {
  const { passwordInput, getByTestId } = setup();
  const value = 'pwd';
  const expected = 'Need a strong password, use at least one upper case letter, 7-14 characters';
  fireEvent.change(passwordInput, { target: { value: value } });

  expect(passwordInput.value).toBe(value);
  expect(getByTestId('errorMessagePassword').textContent).toBe(expected);
});

test('Login Button: should enable signing in if user types a valid emails as username and a valid password', () => {
  const { emailInput, passwordInput, getByTestId, queryByTestId } = setup();
  const inputValues = {
    email: 'test@gmail.com',
    password: 'Passw0rd'
  };
  fireEvent.change(emailInput, { target: { value: inputValues.email } });
  fireEvent.change(passwordInput, { target: { value: inputValues.password } });

  expect(queryByTestId('errorMessageUser')).toBeNull();
  expect(queryByTestId('errorMessagePassword')).toBeNull();
  expect(getByTestId('submitButton').disabled).toBe(false);
});

test('Login Button: should disable signing in if user types invalid emails as username or invalid password', () => {
  const { emailInput, passwordInput, getByTestId } = setup();
  const inputValues = {
    email: 'test',
    password: 'Pwd'
  };
  const expectedResults = {
    errorMessageUser: 'Invalid email address',
    errorMessagePassword: 'Need a strong password, use at least one upper case letter, 7-14 characters'
  }
  fireEvent.change(emailInput, { target: { value: inputValues.email } });
  fireEvent.change(passwordInput, { target: { value: inputValues.password } });

  expect(getByTestId('errorMessageUser').textContent).toBe(expectedResults.errorMessageUser);
  expect(getByTestId('errorMessagePassword').textContent).toBe(expectedResults.errorMessagePassword);
  expect(getByTestId('submitButton').disabled).toBe(true);
});
