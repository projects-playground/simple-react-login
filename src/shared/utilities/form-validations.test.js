import { hasValidEmail, hasValidPassword } from "./form-validations";

describe("Utilities for form validations", () => {
  it("hasValidEmail() should be true if valid email is passed", () => {
    expect(hasValidEmail("test@gmail.co.uk")).toBe(true);
    expect(hasValidEmail("test@gmail.com")).toBe(true);
    expect(hasValidEmail("test@gmail.com")).toBe(true);
  });

  it("hasValidEmail() should be false if invalid email is passed", () => {
    expect(hasValidEmail("test@gmail")).toBe(false);
    expect(hasValidEmail("test")).toBe(false);
  });

  it("hasValidPassword() should be true if valid password is passed", () => {
    expect(hasValidPassword("testa345D")).toBe(true);
    expect(hasValidPassword("Passw0rd")).toBe(true);
  });

  it("hasValidPassword() should be false if invalid password is passed", () => {
    expect(hasValidPassword("pwd")).toBe(false);
    expect(hasValidPassword("pW")).toBe(false);
    expect(hasValidPassword("PASWORD")).toBe(false);
  });
});
