export const hasValidEmail = (email) => {
  const testEmail = /^[ ]*([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})[ ]*$/i;
  return !!email.match(testEmail);
};

export const hasValidPassword = (password) => {
  // I know this a weak password regex, but also this is not production code
  const testPassword = /^[A-Za-z]\w{7,14}$/;
  return !!password.match(testPassword);
};
