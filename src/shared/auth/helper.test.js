import { hasAccount } from './helper';

fdescribe('Helper', () => {
    const mockUsers = [
        { username: "test@gmail.com", password: "Test1234" },
        { username: "test2@gmail.com", password: "Test2345" }
    ];

    it("hadAccount: should find existing user", () => {
        const mockUser = { username: "test@gmail.com", password: "Test1234" };

        expect(hasAccount(mockUsers, mockUser)).toBe(true);
    })

    it("hadAccount: should not find user if incorrect email is introduced", () => {
        const mockUser = { username: "testt@gmail.com", password: "Test1234" };

        expect(hasAccount(mockUsers, mockUser)).toBe(false);
    })

    it("hadAccount: should not find user if incorrect password is introduced", () => {
        const mockUser = { username: "test@gmail.com", password: "TTest1234" };

        expect(hasAccount(mockUsers, mockUser)).toBe(false);
    })

    it("hadAccount: should not find user if both user and password are incorrect", () => {
        const mockUser = { username: "wrong@gmail.com", password: "wrong password" };

        expect(hasAccount(mockUsers, mockUser)).toBe(false);
    })
});