import { useState, useEffect } from "react";
import "./LoginPage.css";
import {
  hasValidEmail,
  hasValidPassword
} from "../../shared/utilities/form-validations";
import { useAuth } from '../../shared/auth/auth-context';
import { useHistory } from 'react-router-dom';

const LoginPage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isUserValid, setIsUserValid] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);
  const [isFormPristine, setIsFormPristine] = useState(true);
  const { state, dispatch } = useAuth();
  const history = useHistory();

  useEffect(() => {
    setIsFormValid(isUserValid && isPasswordValid);
  }, [isUserValid, isPasswordValid]);

  useEffect(() => {
    if (state.isLoggedIn) {
      history.push('/merchant-list');
    }
  }, [state.isLoggedIn])

  const handleInputChange = (event, setValue, setIsValid, hasValidInput) => {
    const value = event.target.value;
    setValue(value);
    setIsValid(hasValidInput(value));
    setIsFormPristine(false);
  };

  const handleEmailChange = (event) => {
    handleInputChange(event, setUsername, setIsUserValid, hasValidEmail);
  };

  const handlePasswordChange = (event) => {
    handleInputChange(event, setPassword, setIsPasswordValid, hasValidPassword);
  };

  const isInputValid = (validator) => validator || isFormPristine;
  const getInputClass = (validator) =>
    isInputValid(validator) ? "form-control" : "form-control is-invalid";

  const login = (event) => {
    event.preventDefault();
    dispatch({
      type: 'login',
      payload: {
        username,
        password
      }
    })
  }

  return (
    <form
      className={isFormPristine ? "form-signin" : "form-signin not-pristine"}
    >
      <img
        alt=""
        className="mb-4"
        height="72"
        src="https://getbootstrap.com/docs/4.4/assets/brand/bootstrap-solid.svg"
        width="72"
      />

      <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label className="sr-only" htmlFor="inputUsername">
        Username
      </label>
      <input
        type="email"
        value={username}
        onChange={handleEmailChange}
        className={getInputClass(isUserValid)}
        id="inputUsername"
        data-testid="inputUsername"
        placeholder="Username"
      />
      {isInputValid(isUserValid) || (
        <p data-testid="errorMessageUser">Invalid email address</p>
      )}
      <label className="sr-only" htmlFor="inputPassword">
        Password
      </label>
      <input
        value={password}
        onChange={handlePasswordChange}
        className={getInputClass(isPasswordValid)}
        id="inputPassword"
        data-testid="inputPassword"
        placeholder="Password"
        type="password"
      />
      {isInputValid(isPasswordValid) || (
        <p data-testid="errorMessagePassword">
          Need a strong password, use at least one upper case letter, 7-14
          characters
        </p>
      )}

      <div className="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" /> Remember me
        </label>
      </div>

      <p className="error-message" data-testid="errorMessageLogin">{state.errorMessage}</p>

      <button
        data-testid="submitButton"
        disabled={!isFormValid}
        className="btn btn-lg btn-primary btn-block"
        type="submit"
        onClick={login}
      >
        Sign in
      </button>

      <p className="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
    </form>
  );
};

export default LoginPage;
