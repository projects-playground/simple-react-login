import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Header from '../Header';
import HomePage from '../../pages/home';
import MerchantListPage from '../../pages/merchant-list';
import LoginPage from '../../pages/login/LoginPage';
import { useAuth } from '../auth/auth-context';

export const Router = () => {
    const { state } = useAuth();
    return (
        <BrowserRouter>
            <Header />
            <Route path="/login" component={LoginPage} />
            {
                state.isLoggedIn ?
                    <Route path="/merchant-list" component={MerchantListPage} /> :
                    <Redirect path="/" component={HomePage} />
            }
            <Route path="/" exact component={HomePage} />
        </BrowserRouter>
    )
}