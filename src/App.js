import "./styles.css";
import { AuthProvider } from './shared/auth/auth-context';
import { Router } from './shared/Router/Router';

const App = () => {
  return (
    <AuthProvider>
      <Router />
    </AuthProvider>
  );
};

export default App;
